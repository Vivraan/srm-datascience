 
import numpy as np
from nltk.corpus import stopwords

import nltk
nltk.download("stopwords")

#display all the stop words
#print(  set(stopwords.words('greek'))  )
print(  set(stopwords.words('english'))  )

from google.colab import drive
drive.mount('/content/drive')

ls drive/'My Drive'

#word embedding model
from gensim.models import KeyedVectors

def load_word2vec(data):
  #from gensim.models import KeyedVectors
  # load the google word2vec model
  #filename = './GoogleNews-vectors-negative300.bin'
  model = KeyedVectors.load_word2vec_format(data, binary=True)
  # calculate: (king - man) + woman = ?
  return model

data="/content/drive/My Drive/GoogleNews-vectors-negative300.bin.gz"
traindata= "/content/drive/My Drive/data_10k.tsv"

  
model=load_word2vec(data)
  

model.most_similar("question")
model.most_similar("love")
 
print(model.most_similar(positive=['boys','love'] , topn=5))
 
 
# find odd items given a list of items 
model.doesnt_match(["engineering","collage","department"])

len(model.vocab)

 
len( model["immigration"]  )

stops = set(stopwords.words('english'))

def text_to_word_list(text):
    ''' Pre process and convert texts to a list of words '''
    text = str(text)
    text = text.lower()

    ### Clean the text ###
    text = re.sub(r"[^A-Za-z0-9^,!.\/'+-=]", " ", text)
    
    ##apostrophe/punctuation TO abbreviation
    text = re.sub(r"what's", "what is ", text)
    text = re.sub(r"\'s", " ", text)
    text = re.sub(r"\'ve", " have ", text)
    text = re.sub(r"can't", "cannot ", text)
    text = re.sub(r"n't", " not ", text)
    text = re.sub(r"i'm", "i am ", text)
    text = re.sub(r"\'re", " are ", text)
    text = re.sub(r"\'d", " would ", text)
    text = re.sub(r"\'ll", " will ", text)
    
    #REPLACE the special char into space
    text = re.sub(r",", " ", text)
    text = re.sub(r"\.", " ", text)
    text = re.sub(r"!", " ! ", text)
    text = re.sub(r"\/", " ", text)
    text = re.sub(r"\^", " ^ ", text)
    text = re.sub(r"\+", " + ", text)
    text = re.sub(r"\-", " - ", text)
    text = re.sub(r"\=", " = ", text)
    text = re.sub(r"'", " ", text)
    
    #
    text = re.sub(r"(\d+)(k)", r"\g<1>000", text)
    text = re.sub(r":", " : ", text)
    text = re.sub(r" e g ", " eg ", text)
    text = re.sub(r" b g ", " bg ", text)
    text = re.sub(r" u s ", " american ", text)
    text = re.sub(r"\0s", "0", text)
    text = re.sub(r" 9 11 ", "911", text)
    text = re.sub(r"e - mail", "email", text)
    text = re.sub(r"j k", "jk", text)
    text = re.sub(r"\s{2,}", " ", text)

    text = text.split()

    return text

import re
text_to_word_list("hi i've pen")

#for visuvalization moduels
import matplotlib.pyplot as plt
import seaborn as sns

import itertools
import datetime

from keras.preprocessing.sequence import pad_sequences
from keras.models import Model
from keras.layers import Input, Embedding, LSTM, lambda
import keras.backend as K
from keras.optimizers import Adadelta
from keras.callbacks import ModelCheckpoint
import re
from nltk.corpus import stopwords
from sklearn.model_selection import train_test_split
from time import time
import pandas as pd

df = pd.read_csv(traindata, sep='\t')

df.head(6)

len(df)

df.shape

import matplotlib.pyplot as plt

def eda(data):
    dup_check = data['is_duplicate'].value_counts()
    plt.bar(dup_check.index, dup_check.values)
    plt.ylabel('Number of Queries')
    plt.xlabel('Is Duplicate')
    plt.title('Data Distribution', fontsize = 18)
    plt.show()
    
    print("\nAbove Graph Features :  [Is Not Duplicate | Is Duplicate]\n")
    print("Above Graph Indices  : ", dup_check.index)
    print("\nAbove Graph Values   : ", dup_check.values)

eda(df)

 
#to split train and test data, from given dataset
msk = np.random.rand(len(df)) < 0.8
train_df= df[msk]
test_df= df[~msk]
len(train_df)

#getting understanding of Python pandas
type(df)
df.dtypes

df.isna()
#df[df.isnull()]
#slicing DF using single column
df['question1']
#slicing DF using multiple column
df[['question1', 'question2']][1:3]

 

df1 = df
# Cleaning and tokenizing the queries.
#Ignoring the CASE[lower = upper]
words = re.compile(r"\w+",re.I)

stopword = stopwords.words('english')

#Stemming, remove the extra char from words. # LancasterStemmer is alternative to PorterStemmer
from nltk.stem import PorterStemmer
stemmer = PorterStemmer()
stemmer.stem('meeting') #coming ... 
stemmer.stem('examples')


def tokenize_questions(df):
    question_1_tokenized = []
    question_2_tokenized = []

    for q in df.question1.tolist():
        question_1_tokenized.append([stemmer.stem(i.lower()) for i in words.findall(q) 
                                     if i not in stopword])

    for q in df.question2.tolist():
        question_2_tokenized.append([stemmer.stem(i.lower()) for i in words.findall(q) 
                                     if i not in stopword])

    df1["Question_1_tok"] = question_1_tokenized
    df1["Question_2_tok"] = question_2_tokenized
    
    return df1

df1_tokenize = tokenize_questions(df1)
df1_tokenize.head()

#df1.columns
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
# Start with 1 questions:
text = df1.question1[0]

# Create and generate a word cloud image:
wordcloud = WordCloud().generate(text)

# Display the generated image:
plt.imshow(wordcloud, interpolation='bilinear')
plt.axis("off")
plt.show()

# lower max_font_size, change the maximum number of word and lighten the background:
wordcloud = WordCloud(max_font_size=50, max_words=100, background_color="white").generate(text)
plt.figure()
plt.imshow(wordcloud, interpolation="bilinear")
plt.axis("off")
plt.show()

 
#now you'll combine all wine questions into one big text and create a big fat cloud to see which characteristics are most common questions.
text = " ".join(review for review in df1.question1[0:50])
print ("There are {} words in the combination of all review.".format(len(text)))

# Create stopword list:
stopwords = set(STOPWORDS)
stopwords.update(["question", "now", "today", "flavor", "flavors"])

# Generate a word cloud image
wordcloud = WordCloud(stopwords=stopwords, background_color="white").generate(text)

# Display the generated image:
# the matplotlib way:
plt.imshow(wordcloud, interpolation='bilinear')
plt.axis("off")
plt.show()

    
vocabulary = dict()
inverse_vocabulary = ['<unk>'] 
word2vec=model
questions_cols = ['question1', 'question2']

for dataset in [train_df, test_df]:
    for index, row in dataset.iterrows():

        # Iterate through the text of both questions of the row
        for question in questions_cols:

            q2n = []  # q2n -> question numbers representation
            for word in text_to_word_list(row[question]):

                # Check for unwanted words
                if word in stops and word not in word2vec.vocab:
                    continue

                if word not in vocabulary:
                    vocabulary[word] = len(inverse_vocabulary)
                    q2n.append(len(inverse_vocabulary))
                    inverse_vocabulary.append(word)
                else:
                    q2n.append(vocabulary[word])

            # Replace questions as word to question as number representation
            dataset.set_value(index, question, q2n)

type(dataset)
dataset[1:10]

vocabulary['step']
inverse_vocabulary[4]

word2vec = model
embedding_dim = 300
 
embeddings = 1 * np.random.randn(len(vocabulary) + 1, embedding_dim)  # This will be the embedding matrix
##This 0 to be padded for <unk> //unknown values, all the 300 rows will have 0 values.
embeddings[0] = 0  # So that the padding will be ignored

# Build the embedding matrix
for word, index in vocabulary.items():
    if word in word2vec.vocab:
        embeddings[index] = word2vec.word_vec(word)

del word2vec

embeddings

 
#each question will have diff tokens and size., find the max number of tokens from all the data

max_seq_length = max(train_df.question1.map(lambda x: len(x)).max(),
                     train_df.question2.map(lambda x: len(x)).max(),
                     test_df.question1.map(lambda x: len(x)).max(),
                     test_df.question2.map(lambda x: len(x)).max())

# Split to train validation
##This value is for Original source data with around 3Laks training set
#validation_size = 40000
##This value is for demo source data with around 10K training set
validation_size = 4000
training_size = len(train_df) - validation_size

X = train_df[questions_cols]
X
Y = train_df['is_duplicate']
Y
X_train, X_validation, Y_train, Y_validation = train_test_split(X, Y, test_size=validation_size)

# Split to dicts
X_train = {'left': X_train.question1, 'right': X_train.question2}
X_validation = {'left': X_validation.question1, 'right': X_validation.question2}
X_test = {'left': test_df.question1, 'right': test_df.question2}

# Convert labels to their numpy representations
Y_train = Y_train.values
Y_validation = Y_validation.values

# Zero padding
for dataset, side in itertools.product([X_train, X_validation], ['left', 'right']):
    dataset[side] = pad_sequences(dataset[side], maxlen=max_seq_length)

# Make sure everything is ok
assert X_train['left'].shape == X_train['right'].shape
assert len(X_train['left']) == len(Y_train)





n_hidden = 50
gradient_clipping_norm = 1.25
batch_size = 64
n_epoch = 2

def exponent_neg_manhattan_distance(left, right):
    ''' Helper function for the similarity estimate of the LSTMs outputs'''
    return K.exp(-K.sum(K.abs(left-right), axis=1, keepdims=True))

# The visible layer
left_input = Input(shape=(max_seq_length,), dtype='int32')
right_input = Input(shape=(max_seq_length,), dtype='int32')

embedding_layer = Embedding(len(embeddings), embedding_dim, weights=[embeddings], input_length=max_seq_length, trainable=False)

# Embedded version of the inputs
encoded_left = embedding_layer(left_input)
encoded_right = embedding_layer(right_input)
#print("Training time finished.\n{} epochs in {}".format(n_epoch, datetime.timedelta(seconds=time()-training_start_time)))
# Since this is a siamese network, both sides share the same LSTM
shared_lstm = LSTM(n_hidden)
optimizer = Adadelta(clipnorm=gradient_clipping_norm)

left_output = shared_lstm(encoded_left)
right_output = shared_lstm(encoded_right)

# Calculates the distance as defined by the MaLSTM model
malstm_distance = Lambda(function=lambda x: exponent_neg_manhattan_distance(x[0], x[1]),output_shape=lambda x: (x[0][0], 1))([left_output, right_output])

malstm = Model([left_input, right_input], [malstm_distance])
# def cosine_distance(vests):
#     x, y = vests
#     x = K.l2_normalize(x, axis=-1)
#     y = K.l2_normalize(y, axis=-1)
#     return -K.mean(x * y, axis=-1, keepdims=True)

# def cos_dist_output_shape(shapes):
#     shape1, shape2 = shapes
#     return (shape1[0],1)

# cosine_distance = Lambda(cosine_distance, output_shape=cos_dist_output_shape)([processed_a, processed_b])
# malstm = Model([left_input, right_input], [coine_distance])
#cosine value of two word vector be ~ [-1,1] (future reminder projectioning the each vector to orthoganal vector  gives [0,1]) 

malstm.compile(loss='mean_squared_error', optimizer=optimizer, metrics=['accuracy'])
# ltraining_start_time = time()
malstm_trained = malstm.fit([X_train['left'], X_train['right']], Y_train, batch_size=batch_size, nb_epoch=n_epoch,
                            validation_data=([X_validation['left'], X_validation['right']], Y_validation))
history_details=malstm_trained 
filename="history.pickle"
import pickle
with open(filename, 'wb') as f:a
    pickle.dump(history_details, f)

print(malstm_trained.history)
# with open('history.pickle', 'rb') as f:
#    a = pickle.load(f)
# print(a)

plt.clf()
history_dict=malstm_trained.history
acc_values=history_dict["acc"]
val_acc= history_dict["val_acc"]
epochs=range(1,len(history_dict["acc"])+1)
plt.plot(epochs,acc_values,"r",label="Training_accuracy")
plt.plot(epochs,val_acc,"b",label="validation accuracy")
plt.title("training and validation accuracy")
plt.xlabel("Epoches")
plt.ylabel("accuracy")
plt.legend()
plt.show()

plt.clf()
history_dict=malstm_trained.history
loss_values=history_dict["loss"]
val_loss= history_dict["val_loss"]
epochs=range(1,len(history_dict["loss"])+1)
plt.plot(epochs,loss_values,"r",label="Training_loss")
plt.plot(epochs,val_loss,"b",label="validation loss")
plt.title("training and validation loss")
plt.xlabel("Epoches")
plt.ylabel("loss")
plt.legend()
plt.show()

#save weights, for futureuse
from keras.models import load_model

malstm.save_weights("/content/drive/My Drive/nlp_new/lstm/word_similarity/savd_model/model_weights_3L.h5")
malstm.save("/content/drive/My Drive/nlp_new/lstm/word_similarity/savd_model/malstm_model_3L.h5")

